package com.voicerecorder;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class VoiceRecord extends Activity 
{
	private Button play,start,stop,save,discard;
	boolean flip=true;
	MediaPlayer mp;
	String directoryPath;
	AudioRecorder audio_recorder;
	String PATH_NAME="";
	Calendar calender=null;
	String file_name;
	Button btnPlay,btnStop; 
	TextView textViewTime;
	Thread timeThread;
    int hour=0,minute=0,time=0,minut_chek=0;
	int flag=0;
	TimerTask mTimerTask;
	final Handler handler = new Handler();
	Timer t = new Timer();	
	int player_variable=0;
	public RelativeLayout layoutToAdd;
    File path_to_delete;
    String path_of_file;
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.voicerecord);
   
        layoutToAdd = (RelativeLayout) findViewById(R.id.relativeLayout1);
       
        start=(Button)findViewById(R.id.record);
        stop=(Button)findViewById(R.id.stop);
        save=(Button)findViewById(R.id.button2);
        play=(Button)findViewById(R.id.button3);
        discard=(Button)findViewById(R.id.button1);
		textViewTime=(TextView)findViewById(R.id.time);
      	play.setVisibility(View.GONE);
		discard.setVisibility(View.GONE);	
		save.setVisibility(View.GONE);
		
    }
   public void createFile()
   {
	   file_name=new SimpleDateFormat("HHmmss").format(new Date());;
	   PATH_NAME="/Audio_Recorded_Files/"+file_name;
	   audio_recorder = new AudioRecorder(PATH_NAME);
	   path_of_file=Environment.getExternalStorageDirectory().getAbsolutePath()+PATH_NAME+".mp3";
   }

  public void onStart()
  {
	  super.onStart();
	
	 play.setOnClickListener(new OnClickListener() 
      {			
			public void onClick(View v)
			{

				
			    if(player_variable==0)
			    {
					String str=Environment.getExternalStorageDirectory().getAbsolutePath()+PATH_NAME+".mp3";
					hour=0;
		      	 	minute=0;
		      	 	time=0;
			    	mp=new MediaPlayer();
			    	
					try 
					{
						mp.setDataSource(str);
						mp.prepare();
						
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (SecurityException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalStateException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			    }
   			       flip=!flip;		
					if(flip)
					{
							mp.pause();	
							stopTask();
							play.setBackgroundResource(R.drawable.play);
							save.setEnabled(true);
							save.setBackgroundResource(R.drawable.save);
						    discard.setEnabled(true);;
						    discard.setBackgroundResource(R.drawable.cancel);
						
					}	
					else 
					{
								player_variable=1;
								 
								if(!mp.isPlaying())
								{
									mp.start();
									doTimerTask();
								}
								else
								{
									 mp.start();
									
									
								}
								
								save.setEnabled(false);
								save.setBackgroundResource(R.drawable.save1);
							    discard.setEnabled(false);;
							    discard.setBackgroundResource(R.drawable.cancel1);
														
								play.setBackgroundResource(R.drawable.pause);
								
					}
     			}
		
      
		});
	 
	  start.setOnClickListener(new OnClickListener() 
       {			
			public void onClick(View v)
			{
				
        		

				start.setEnabled(false);
				start.setBackgroundResource(R.drawable.record1);
			    stop.setEnabled(true);;
			    stop.setBackgroundResource(R.drawable.stop);
				hour=0;
	      	 	minute=0;
	      	 	time=0;
			    createFile();
				 
				
				try {

						flag=1;
					    audio_recorder.start();
						minut_chek=0;
						doTimerTask();
						
					} catch (IOException e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				  
					
			}

			
			
			
      });	
			
	  
	  stop.setOnClickListener(new OnClickListener() 
      {			
			public void onClick(View v)
			{
	
				start.setVisibility(View.GONE);
				stop.setVisibility(View.GONE);
				play.setVisibility(View.VISIBLE);
				discard.setVisibility(View.VISIBLE);	
				save.setVisibility(View.VISIBLE);
        		layoutToAdd.setVisibility(View.VISIBLE);
			    stop.setEnabled(false);;
			    stop.setBackgroundResource(R.drawable.stop1);
   			
  			   try
  			  {
					if(flag==1)
					{
						minut_chek=stopTask();
						
						audio_recorder.stop();
						

					}
					else
					{
						Toast.makeText(getBaseContext(),"Till now you have not started recording",Toast.LENGTH_SHORT).show();
					}
		        	} catch (IOException e)
		        	{
				// TODO Auto-generated catch block
		        		e.printStackTrace();
		        	} 
				
			}

			
		});
	  save.setOnClickListener(new View.OnClickListener()
      {
			
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				Toast.makeText(getBaseContext(),"Sucessfully Saved",Toast.LENGTH_SHORT).show();
				start.setVisibility(View.VISIBLE);
				stop.setVisibility(View.VISIBLE);
				play.setVisibility(View.GONE);
				discard.setVisibility(View.GONE);	
				save.setVisibility(View.GONE);
			    start.setEnabled(true);;
			    start.setBackgroundResource(R.drawable.record);
			    textViewTime.setText("0"+"0"+":"+"0"+"0"+":"+"0"+"0");
			}
		});
	
      discard.setOnClickListener(new View.OnClickListener() 
      {
			
			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				path_to_delete=new File(path_of_file);
				if(path_to_delete.exists())
				{
					path_to_delete.delete();
					start.setVisibility(View.VISIBLE);
					stop.setVisibility(View.VISIBLE);
					play.setVisibility(View.GONE);
					discard.setVisibility(View.GONE);	
					save.setVisibility(View.GONE);
				    start.setEnabled(true);;
				    start.setBackgroundResource(R.drawable.record);
				    textViewTime.setText("0"+"0"+":"+"0"+"0"+":"+"0"+"0");

				}
			
			}
			
		});
  }
  private void doTimerTask()
	{
		// TODO Auto-generated method stub
		    	mTimerTask = new TimerTask() 
		    	{
		    	        public void run() 
		    	        {
		    	                handler.post(new Runnable()
		    	                {
		    	                        public void run() 
		    	                        {
	    	                        		
	    	                        		time=time+1;
		    	                        	int value_check=0;
		    	                        	if(time<10&&minute<10&&hour<10)
		    	                        	{		    	                        		
		    	                        	
		    	                        	  textViewTime.setText("0"+hour+":"+"0"+minute+":"+"0"+time+"");
		    	                        	  value_check=time;
		    	                        	

		    	                        	 }
		                                        // update TextView
		    	                        	else
		    	                        	{
			    	                        	 			    	                        	  
		    	                        	  if(time%60==0)
			    	                          {	
			    	                        	minute+=1;
			    	                        	time=1;
			    	                        	 if(minute%60==0)
			    	                        	   {
			    	                        	    hour+=1;
			    	                        	    minute=1;
			    	                               }	
			    	                           }
		    	                        	  
		    	                        	  if(minute<10&&hour<10)
		    	                        	    {
				    	                          textViewTime.setText("0"+hour+":"+"0"+minute+":"+time+"");
		    	                        	    }
		    	                        	   else 
		    	                        	    {
		    	                        		  textViewTime.setText(hour+":"+minute+":"+time+"");
		    	                        	    }
		    	                        	
		    	                        	
		    	                        	} 
		    	                        	int time_minut=minute;
		    	                        	int time_hour=hour;
		    	                        value_check=time+time_minut*60+time_hour*3600; 
			    	     
		    	                        if(minut_chek>0&&value_check>=minut_chek)
		    	                        {
		    	                        	mp.pause();	
		    	                        	mp.stop();
		    	                        	mp.release();
		    								stopTask();
		    								play.setBackgroundResource(R.drawable.play);
		    								save.setEnabled(true);
		    								save.setBackgroundResource(R.drawable.save);
		    							    discard.setEnabled(true);;
		    							    discard.setBackgroundResource(R.drawable.cancel);
		    								flip=true;
		    								player_variable=0;
		    							    textViewTime.setText("0"+"0"+":"+"0"+"0"+":"+"0"+"0");

		    						
		    	                        }
		    	                        	
		    	                        }
		    	               });
		    	        }};
		 
		            // public void schedule (TimerTask task, long delay, long period) 
		    	    t.schedule(mTimerTask, 1000, 1000);  // 
		 
		    	 
	}
  private int stopTask() 
	{
		// TODO Auto-generated method stub
	  int last_time=0;
	  int time_minut=minute;
      int time_hour=hour;
		if(mTimerTask!=null)
		{
		if(time<10)
      	{
			
      	 	last_time=time+time_minut*60+time_hour*3600;
       	 	mTimerTask.cancel();
      	}
              // update TextView
      	else
      	{
      	
            	mTimerTask.cancel();
      	 	last_time=time+time_minut*60+time_hour*3600;
      	}
		

  	  }
		return last_time;
	}
}
